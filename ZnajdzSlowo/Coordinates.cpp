#include "Coordinates.h"

void Coordinates::findWordVector(int start_row, int start_column, int in_len, int in_x, int in_y)
{
    // Create x and y vectors based on word search direction
    while (in_len--)
    {
        vec_x.push_back(start_row + in_x * in_len);
        vec_y.push_back(start_column + in_y * in_len);
    }
}

bool Coordinates::matchCoordinates(int in_row, int in_column, int in_len)
{
    auto it = find(vec_x.begin(), vec_x.end(), in_row);
    auto it_2 = find(vec_y.begin(), vec_y.end(), in_column);

    // Check if specific row and column coordinate is part of vec_x or vec_y
    if ((it != vec_x.end() && it_2 != vec_y.end()))
    {
        while (in_len--)
            if ((in_row == vec_x.at(in_len)) && (in_column == vec_y.at(in_len)))

                return true;

        return false;
    }
    else

        return false;
}