#pragma once
#include "Position.h"
#include <vector>

class Coordinates
{
public:
    // Store directions
    struct Position directions[6] = { {-1, -1}, {-1, 0}, {-1, 1}, {1, -1}, {1, 0}, {1, 1} };

    // This function finds vectors that store found word letters
    void findWordVector(int start_row, int start_column, int in_len, int x, int y);
    // This function checks if direction vectors are in match
    bool matchCoordinates(int row, int column, int in_len);

private:
    // Vectors to store found word coordinates
    std::vector<int>vec_x;
    std::vector<int>vec_y;
};