#include <iostream>
#include "Coordinates.h"
#include "Position.h"
#include <fstream>
#include <string>
#include <iterator>
#include <vector>
#include <algorithm> 

using namespace std;

//  This finds word pattert in grid
void patternSearch(vector<string> in_grid, string in_word);

// This function searches in all possible direction from point 
void searchGrid(vector<string> in_grid, bool &out_word_found, int row, int in_x_border, int column, int in_y_border, string word);

//  This function prints results to terminal
void printResults(vector<string> in_grid, int start_row, int start_column, int in_len, int x, int y);

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        cout << "\nUzycie: ZnajdzSlowo.exe <nazwapliku.txt>\n";
        return -1;
    }

    char *file_name = argv[1];

    ifstream inFile(file_name);
    if (!inFile)
    {
        cout << "\nNie znaleziono pliku " << file_name << endl;
        exit(1);
    }
    istream_iterator<string> start(inFile), end;
    vector<string> grid(start, end);
    inFile.close();

    string word;
    char decide = 'y';

    while ((decide == 'Y') || (decide == 'y'))
    {
        system("CLS");
        cout << "Podaj szukane slowo: ";
        cin >> word;
        transform(word.begin(), word.end(), word.begin(), ::toupper);
        patternSearch(grid, word);
        cout << "\nChcesz kontunuowac szukanie? (Y/N): ";
        cin >> decide;
    }

    return 0;
}

void patternSearch(vector<string> in_grid, string in_word)
{
    // Check grid borders
    int x_border = in_grid.size();
    int y_border = in_grid[0].size();

    // Variable to check if word was found
    bool word_found = false;

    // Consider every point as starting point 
    for (int column = 0; column < y_border; column++)
    {
        for (int row = 0; row < x_border; row++)
        {
            searchGrid(in_grid, word_found, row, x_border, column, y_border, in_word);

            // Comunicate that word wasn't found
            if (row == x_border - 1 && column == y_border - 1 && !word_found)
                cout << "\nNie znaleziono slowa " << in_word << endl;
        }
    }
}

void searchGrid(vector<string> in_grid, bool &out_word_found, int in_row, int in_x_border, int in_column, int in_y_border, string word)
{
    // Return if in_grid[row][column] 
    // isn't starting point
    if (in_grid[in_row][in_column] != word[0])
        return;

    // Initialize class Coordinates
    // and state starting row and column
    Coordinates coordinates;
    int len = word.length();
    int start_row = in_row;
    int start_column = in_column;

    // Search word in all possible (6) directions starting from (start_row,start_column) 
    for (int dir = 0; dir < 6; dir++)
    {
        // Initialize starting point for current direction 
        int k;
        int row_dir = in_row + coordinates.directions[dir].x;
        int col_dir = in_column + coordinates.directions[dir].y;

        // Match remaining characters positions 
        for (k = 1; k < len; k++)
        {
            // If out of bound break 
            if (row_dir >= in_x_border || row_dir < 0 || col_dir >= in_y_border || col_dir < 0)

                break;

            // If not matched, break 
            if (in_grid[row_dir][col_dir] != word[k])

                break;

            // Move in every possible direction 
            row_dir += coordinates.directions[dir].x;
            col_dir += coordinates.directions[dir].y;
        }

        // If all character matched, continue
        // to see if this point isn't next starting point
        if (k == len)
        {
            out_word_found = true;
            printResults(in_grid, start_row, start_column, len, coordinates.directions[dir].x, coordinates.directions[dir].y);

            continue;
        }
    }
}

void printResults(vector<string> in_grid, int in_start_row, int in_start_column, int in_len, int in_x, int in_y)
{
    // Initialize class Coordinates
    // and find word letters coordinates to be stored in vectors
    Coordinates coordinates;
    coordinates.findWordVector(in_start_row, in_start_column, in_len, in_x, in_y);

    // Print results to the screen and color
    // found word red
    cout << endl;
    for (size_t row = 0; row < in_grid.size(); row++)
    {
        for (size_t column = 0; column < in_grid[row].size(); column++)
        {
            if (coordinates.matchCoordinates(row, column, in_len))
            {
                cout << "\033[1;31m" << in_grid[row][column] << "\033[0m";
            }
            else
                cout << in_grid[row][column];
        }
        cout << endl;
    }
}