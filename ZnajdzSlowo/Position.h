#pragma once

// Struct to store direction coordinates
struct Position
{
    int x;
    int y;
};